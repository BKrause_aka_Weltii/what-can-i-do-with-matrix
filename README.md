# What can I do with Matrix

## Matrix protocol

- What is matrix
- transition to the idea

## The Idea

- What software can be implemented besides normal chats
    - Surveys
    - Money Management / Split your payments
    - ...
- The software should only use the matrix protocol and no other backend service

### Combine chat and other software

Why not implement a chat software which combines all ideas from above with plugins?

## Prototype

- Web Prototype

### Requirements

- Basic chat
- Plugin API to add functionality like surveys or money splitter
    - No plugin store, hard coded plugins are enough
- Plugins as WebWorker?

### Technical Planning