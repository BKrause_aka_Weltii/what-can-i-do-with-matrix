import { Message } from "./message";

export interface MessageProtocolAdapter {
  onMessageReceive(message: Message): void;
}
