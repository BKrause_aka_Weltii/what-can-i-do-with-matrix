export interface Message {
  getContent(): string;
  getType(): string;
}
