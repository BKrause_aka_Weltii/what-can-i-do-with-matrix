import { Message } from "./message";

export class MatrixMessage implements Message {
  constructor(private content: string, private type: string) {}
  
  getContent(): string {
    return this.content;
  }

  getType(): string {
    return this.type;
  }
}
