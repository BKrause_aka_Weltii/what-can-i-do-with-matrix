// @ts-ignore
import sdk from "matrix-js-sdk";
import { Config } from "./config";
import { CustomWindow } from "./custom-window";
import { MatrixMessage } from "./messaging/matrix-message";
import { PluginApi } from "./plugin/plugin-api";
import { PluginElementFactory } from "./plugin/plugin-element-factory";
import { TestPlugin } from "./plugin/plugin-example/test-plugin/test-plugin";

declare let window: CustomWindow;

window.PluginApi = new PluginApi();

const testPlugin = new TestPlugin();
window.PluginApi.register(testPlugin);

PluginElementFactory.createAndAppendElementForPlugin(testPlugin);

const chatForm = document.getElementById("chat-form");
chatForm.addEventListener("submit", (event: SubmitEvent) => {
  event.preventDefault();
  console.log(event);
});

let client: any;

client = sdk.createClient({
  baseUrl: Config.baseUrl,
  accessToken: Config.accessToken,
  userId: Config.userId,
  timelineSupport: true,
});

const testRoomId = "!FByfdTMqNMrYTMjFTU:matrix.org";

// client.once("sync", function (state: unknown, prevState: any, res: any) {
//   const rooms: [] = client.getRooms();
//   const room: any = rooms.find((room: any) => room.roomId === testRoomId);
//   const chatContent = document.getElementById("chat-content");
//   for (const message of room.timeline) {
//     if (message.event.type === "m.room.message") {
//       const li = document.createElement("li");
//       li.innerText = message.event.content.body;
//       //  chatContent.append(li);
//     }
//   }
// });

client.on("Room.timeline", function (event: any) {
  const chatContent = document.getElementById("chat-content");
  if (event.getRoomId() === testRoomId) {
    let li;
    switch (event.event.type) {
      case "m.room.message":
        li = document.createElement("li");
        li.innerText = event.event.content.body;
        chatContent.append(li);
        break;
      case testPlugin.getTypeIdentifier():
        li = document.createElement("li");
        li.innerText = `found: ${event.event.content.body}`;
        chatContent.append(li);
        break;
    }

    window.PluginApi.onMessageReceive(
        new MatrixMessage(event.event.content.body, event.event.type)
    );
  }
});

function sendPluginEvent() {
  const content = {
    body: "Something",
    msgType: testPlugin.getTypeIdentifier(),
  };
  client.sendEvent(
    testRoomId,
    testPlugin.getTypeIdentifier(),
    content,
    "",
    (err: unknown, res: unknown) => {
      console.log(err);
      console.log(res);
    }
  );
}

document
  .getElementById("send-test-message")
  .addEventListener("click", sendPluginEvent);

client.startClient();
