import { PluginApi } from "./plugin/plugin-api";

export interface CustomWindow extends Window {
  PluginApi: PluginApi;
}
