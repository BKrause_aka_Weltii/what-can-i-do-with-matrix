import { Message } from "../../../messaging/message";
import { CustomElementInformation, Plugin } from "../../plugin";
import { TestPluginDivElement } from "./test-plugin-div-element";

export class TestPlugin implements Plugin {
  private static domElementName = "test-plugin-div";
  private domElement: TestPluginDivElement;

  getName(): string {
    return "A test plugin";
  }

  getTypeIdentifier(): string {
    return "com.messaging.test-plugin";
  }

  onMessageReceive(message: Message): void {
    this.domElement.addMessage(message);
  }

  getCustomElementInformation(): CustomElementInformation {
    return {
      name: TestPluginDivElement,
      domName: TestPlugin.domElementName,
    };
  }

  getCustomElement(): TestPluginDivElement {
    if (!this.domElement) {
      this.domElement = document.createElement(TestPlugin.domElementName) as TestPluginDivElement;
    }
    return this.domElement;
  }
}
