import { Message } from "../../../messaging/message";
import { Plugin } from "../../plugin";

export class TestPluginDivElement extends HTMLElement {
  private plugin: Plugin;
  private ul: HTMLUListElement;

  constructor() {
    super();

    this.attachShadow({ mode: "open" });
    this.ul = document.createElement("ul");
    this.shadowRoot.appendChild(this.ul);
  }

  public addMessage(message: Message) {
    const li = document.createElement("li");
    li.innerHTML = `Content: ${message.getContent()}<br>Type: ${message.getType()}`;
    this.ul.append(li);
  }

  private render() {
    this.ul.innerHTML = `
Content of the <b>${this.plugin.getName()}</b><br>
Content type <b>${this.plugin.getTypeIdentifier()}</b>
`;
  }
}
