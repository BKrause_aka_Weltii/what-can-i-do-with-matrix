import { MessageEventHandler } from "./message-event-handler";

export interface CustomElementInformation {
  name: CustomElementConstructor;
  domName: string;
}

export interface Plugin extends MessageEventHandler {
  getName(): string;
  getTypeIdentifier(): string;
  getCustomElementInformation(): CustomElementInformation;
  getCustomElement(): HTMLElement
}
