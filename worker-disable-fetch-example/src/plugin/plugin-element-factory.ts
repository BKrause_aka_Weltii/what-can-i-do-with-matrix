import { Plugin } from "./plugin";

export class PluginElementFactory {
  static createAndAppendElementForPlugin(plugin: Plugin) {
    const customElement: HTMLElement = plugin.getCustomElement();

    const headline = document.createElement("h3");
    headline.innerText = plugin.getName();
    const wrapper = document.createElement("div");

    wrapper.append(headline);
    wrapper.append(customElement);

    document.getElementById("plugins").append(wrapper);
  }
}
