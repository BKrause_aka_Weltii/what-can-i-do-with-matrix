import { Message } from "../messaging/message";

export interface MessageEventHandler {
  onMessageReceive(message: Message): void;
}
