import { Message } from "../messaging/message";
import { MessageProtocolAdapter } from "../messaging/message-protocol-adapter";
import { CustomElementInformation, Plugin } from "./plugin";
import { PluginAlreadyRegisteredError } from "./plugin-already-registered-error";

export class PluginApi implements MessageProtocolAdapter {
  private plugins: Map<string, Plugin> = new Map();

  /**
   * @param plugin
   * @throws {PluginAlreadyRegisteredError}
   */
  register(plugin: Plugin): void {
    if (!this.plugins.has(plugin.getTypeIdentifier())) {
      this.plugins.set(plugin.getTypeIdentifier(), plugin);
      this.registerCustomElement(plugin.getCustomElementInformation());
    } else {
      throw new PluginAlreadyRegisteredError(plugin);
    }
  }

  unregister(plugin: Plugin): void {
    this.plugins.delete(plugin.getTypeIdentifier());
  }

  onMessageReceive(message: Message): void {
    const plugin = this.plugins.get(message.getType());
    if (plugin) {
      plugin.onMessageReceive(message);
    }
  }

  private registerCustomElement(customElementData: CustomElementInformation) {
    customElements.define(customElementData.domName, customElementData.name);
  }
}
