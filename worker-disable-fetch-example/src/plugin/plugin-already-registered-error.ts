import { Plugin } from "./plugin";

export class PluginAlreadyRegisteredError extends Error {
  readonly pluginName: string;
  readonly pluginType: string;

  constructor(plugin: Plugin) {
    const pluginName = plugin.getName();
    const pluginType = plugin.getTypeIdentifier();
    super(
      `The plugin ${pluginName} (type identifier ${pluginType}) is already registered.`
    );
    this.pluginName = pluginName;
    this.pluginType = pluginType;
  }
}
