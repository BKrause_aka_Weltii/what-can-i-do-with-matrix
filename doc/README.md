# Concept Documentation

This documentation make use of web application concepts.

## Structure of the Plugin API

![Plugin API Structure](./plugin-api-strucutre.drawio.svg)

## Diagram Style

The basic draw.io dark style includes a strange background configuration, so a other style was chosen.


## Draw.io file Format

Why `.drawio.svg` and not `.drawio` and a exported `.svg`?
The `.drawio.svg` includes a copy of the editable diagram, this way only one file must be maintained.